/******************************************************************************

formEvents() jquery plugin written by Todd Resudek for Supersimple.org
Released May 2010
Based on formEvents js function originally released March 2007

This function will add a default value to all text input elements.

When the user focuses on that input, the value is removed (so that they can
start entering their data), and when they loose focus on that element, the
function will check to see if they left the input blank, if so, it will replace
the default value. If not, the function does nothing.

Usage: This is a good way to label fields for users without having a separate
label element.

This script must be initialized in order to work.
For example, add this to the <head> of your page:
<script type="text/javascript" charset="utf-8">
	$(document).ready(function(){
		$(this).formevents();
	});
</script>

The default value is set by using the title attribute. For any elements that you
want to have a default value, add the title attribute, like this:
<input type="text" title="Enter Your Name" value="" />

If you want dont want a default value, leave out the title attribute.

If you want a title, but still dont want a default value, add the 'noevents'
class to the input, like this:
<input type="text" title="Enter Your Name" class="noevents" value="" />

==============================================================================

License: This script is released under the CC Attribution 3.0 license.
http://creativecommons.org/licenses/by/3.0/us/

You are free to share,copy,distribute,display and remix this script.

You must leave this comment intact. (fair is fair)
	
For any reuse or distribution, you must make clear to others the license terms 
of this work. The best way to do this is with a link to the CC web page.

Any of the above conditions can be waived if you get permission from the
copyright holder.

Apart from the remix rights granted under this license, nothing in this 
license impairs or restricts the author's moral rights.

******************************************************************************/

jQuery.fn.formevents = function() {
  //loop thru each form on the page
	$("input").each(function(index){
			//qualify that the input is a text input, and has a title, and isn't being excluded by using the 'noevents' class
			var thistitle = $(this).attr('title');
			var thistype = $(this).attr('type');
			if(thistitle && thistitle != '' && thistype != 'image' && thistype != 'button' && thistype != 'submit' && !$(this).hasClass('noevents')){
				//assign the title to the value
				$(this).val(thistitle);
				//events that will remove/replace default values into fields
				$(this).focus(function(){ if(thistitle == $(this).val()){$(this).val('');} });
				$(this).blur(function(){ if($(this).val() == ''){$(this).val(thistitle);} });
			}
			
	});
};
